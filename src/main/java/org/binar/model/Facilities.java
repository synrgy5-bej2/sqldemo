package org.binar.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Facilities implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Integer id;

    private Boolean wifi;

    private Boolean bathroom;

    private Boolean security;

    private Boolean pillow;

    private Boolean bed;

    /**
     * One to one dipake buat jelasin relasi antara room dengan facility
     * joinColumn, dipake untuk liat mapping field nya itu ke field mana
     * jsonBackReference, untuk menghindari infinite recursion aja; backReference dipakai buat kelas yg di refer
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "roomId")
    @JsonBackReference
    private Room roomId;
}

package org.binar.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.hypersistence.utils.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class JsonDemo implements Serializable {

    @Id
    private Integer id;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private JsonbColumn test;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
    public static class JsonbColumn {
        @JsonProperty("columnA")
        private String columnA;
        @JsonProperty("columnB")
        private String columnB;
        @JsonProperty("columnC")
        private String columnC;
    }
}

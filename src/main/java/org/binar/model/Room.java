package org.binar.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Room implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Integer id;

    private String roomName;

    private String location;

    /**
     * One to one dipake buat jelasin relasi antara room dengan facility
     * mappedBy, dipake untuk liat mapping field nya itu ke field mana
     * jsonManagedReference, untuk menghindari infinite recursion aja; managedReference dipake di kelas yg ngerefer
     */
    @OneToOne(mappedBy = "roomId",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JsonManagedReference
    private Facilities facilities;
}

package org.binar.repository;

import org.binar.model.JsonDemo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JsonDemoRepository extends JpaRepository<JsonDemo, Integer> {

    List<JsonDemo> findByTest(JsonDemo.JsonbColumn jsonbColumn);

    @Query(value = "select j from json_demo j where j.test @> to_jsonb(CAST :data TO jsonb)", nativeQuery = true)
    List<JsonDemo> findOptionally(@Param("data") String data);

//    List<JsonDemo> findByTestContaining(JsonDemo.JsonbColumn jsonbColumn);
}

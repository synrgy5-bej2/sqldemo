package org.binar.repository;

import org.binar.model.Room;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    @Query(nativeQuery = true, value = "SELECT r FROM room r JOIN facilities f ON f.room_id = r.id")
    List<Room> getAllRoomAndFacilities();

    // Query get room dan facility dengan filter yang dinamis
    @Query(nativeQuery = true, value = "select * from room r\n" +
            "\tjoin facilities f on f.room_id = r.id\n" +
            "\twhere (:wifi is null or f.wifi = :wifi) \n" +
            "\tand (:bathroom is null or f.bathroom = :bathroom) \n" +
            "\tand (:security is null or f.security = :security) \n" +
            "\tand (:pillow is null or f.pillow = :pillow) \n" +
            "\tand (:bed is null or f.bed = :bed)")
    List<Room> getAllRoomFilter(@Param("wifi") Boolean wifi, @Param("bathroom") Boolean bathroom, @Param("security") Boolean security,
                                @Param("pillow") Boolean pillow, @Param("bed") Boolean bed);

    /**
     * Query get room dan facility dengan filter yang dinamis dan juga bisa dikasih pagination beserta sort juga
     * hanya dengan menambahkan Object Pageable
     * INGAT!! Object Pageable harus disimpen di paling ujung
     * @param wifi
     * @param bathroom
     * @param security
     * @param pillow
     * @param bed
     * @param pageable
     * @return
     */
    @Query(nativeQuery = true, value = "select * from room r\n" +
            "\tjoin facilities f on f.room_id = r.id\n" +
            "\twhere (:wifi is null or f.wifi = :wifi) \n" +
            "\tand (:bathroom is null or f.bathroom = :bathroom) \n" +
            "\tand (:security is null or f.security = :security) \n" +
            "\tand (:pillow is null or f.pillow = :pillow) \n" +
            "\tand (:bed is null or f.bed = :bed)")
    List<Room> getAllRoomFilterPageable(@Param("wifi") Boolean wifi, @Param("bathroom") Boolean bathroom, @Param("security") Boolean security,
                                @Param("pillow") Boolean pillow, @Param("bed") Boolean bed,
                                Pageable pageable);
}

package org.binar.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.binar.model.JsonDemo;
import org.binar.repository.JsonDemoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/json-demo")
public class JsonDemoController {

    @Autowired
    JsonDemoRepository jsonDemoRepository;

    @GetMapping("/get/{id}")
    public JsonDemo getJsonDemo(@PathVariable("id") Integer id) {
        return jsonDemoRepository.findById(id).get();
    }

    @GetMapping("/get")
    public List<JsonDemo> getJsonDemoByTest(
            @RequestParam(value = "columnA", required = false) String columnA,
            @RequestParam(value = "columnB", required = false) String columnB,
            @RequestParam(value = "columnC", required = false) String columnC) throws JsonProcessingException {
        String data = new ObjectMapper().writer().writeValueAsString(JsonDemo.JsonbColumn.builder()
                .columnA(columnA)
                .columnB(columnB)
                .columnC(columnC)
                .build());
        return jsonDemoRepository.findOptionally(data);
    }

    @PostMapping("/post")
    public String newJsonDemo(@RequestBody JsonDemo jsonDemo) {
        jsonDemoRepository.save(jsonDemo);
        return "OK";
    }
}

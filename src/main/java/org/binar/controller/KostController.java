package org.binar.controller;

import org.binar.model.Room;
import org.binar.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/kost")
public class KostController {

    @Autowired
    RoomService roomService;

    @GetMapping(value = "/all-kost", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<Room> getAllKost() {
        return roomService.getAllRoom();
    }

    /**
     * get room & facility dengan filter yang dinamis
     * kunci kedinamisan filternya di controller itu ada di required = false
     * @param wifi
     * @param bathroom
     * @param security
     * @param pillow
     * @param bed
     * @return
     */
    @GetMapping(value = "/all-kost/filter")
    public List<Room> allKostFilter(
            @RequestParam(value = "wifi", required = false) Boolean wifi,
            @RequestParam(value = "bathroom", required = false) Boolean bathroom,
            @RequestParam(value = "security", required = false) Boolean security,
            @RequestParam(value = "pillow", required = false) Boolean pillow,
            @RequestParam(value = "bed", required = false) Boolean bed
    ) {
        return roomService.getRoomFilterByFacility(wifi, bathroom, security, pillow, bed);
    }

    /**
     * get room & facility dengan filter yang dinamis dan menggunakan pagination beserta sort
     * kunci kedinamisan filternya di controller itu ada di required = false
     * @param wifi
     * @param bathroom
     * @param security
     * @param pillow
     * @param bed
     * @param page
     * @param size
     * @param field
     * @param direction
     * @return
     */
    @GetMapping(value = "/all-kost/filter-paged")
    public List<Room> allKostFilterPageable(
            @RequestParam(value = "wifi", required = false) Boolean wifi,
            @RequestParam(value = "bathroom", required = false) Boolean bathroom,
            @RequestParam(value = "security", required = false) Boolean security,
            @RequestParam(value = "pillow", required = false) Boolean pillow,
            @RequestParam(value = "bed", required = false) Boolean bed,
            @RequestHeader("page") Integer page,
            @RequestHeader("size") Integer size,
            @RequestParam(value = "field", required = false) String field,
            @RequestParam(value = "direction", required = false) String direction
    ) {
        /**
         * untuk menggunakan pagination bisa pake PageRequest.of(page, size), klo mau sekalian sort juga pake PageRequest.of(page, size, Sort)
         * cara buat Sort nya itu dengan Sort.by(Direction, field)
         * Direction bisa dibuat dengan Sort.Direction.fromString(ASC atau desc)
         */
        return roomService.getRoomFilterByFacilityPageable(wifi, bathroom, security, pillow, bed,
                PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(direction), field)));
    }
}

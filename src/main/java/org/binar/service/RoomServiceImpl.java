package org.binar.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.binar.model.Room;
import org.binar.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RoomServiceImpl implements RoomService {

    @Autowired
    RoomRepository roomRepository;

    @Override
    public List<Room> getAllRoom() {
        return roomRepository.findAll();
    }

    @Override
    public List<Room> getAllRoomAndFacility() {
        return roomRepository.getAllRoomAndFacilities();
    }

    @Override
    public List<Room> getRoomFilterByFacility(Boolean wifi, Boolean bathroom, Boolean security, Boolean pillow, Boolean bed) {
        return roomRepository.getAllRoomFilter(wifi, bathroom, security, pillow, bed);
    }

    @Override
    public List<Room> getRoomFilterByFacilityPageable(Boolean wifi, Boolean bathroom, Boolean security, Boolean pillow, Boolean bed, Pageable pageable) {
        return roomRepository.getAllRoomFilterPageable(wifi, bathroom, security, pillow, bed, pageable);
    }
}

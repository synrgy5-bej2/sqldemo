package org.binar.service;

import org.binar.model.Room;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoomService {

    List<Room> getAllRoom();

    List<Room> getAllRoomAndFacility();

    List<Room> getRoomFilterByFacility(Boolean wifi, Boolean bathroom, Boolean security, Boolean pillow, Boolean bed);

    List<Room> getRoomFilterByFacilityPageable(Boolean wifi, Boolean bathroom, Boolean security, Boolean pillow, Boolean bed, Pageable pageable);
}
